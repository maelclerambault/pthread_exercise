#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/random.h>
#include <time.h>

#define NUM_THREADS 8

struct workload {
    int iteration;
};

struct thread_spec {
    int id;
    int iteration;
    pthread_t thread_id;
    pthread_mutex_t work_mutex;
    pthread_cond_t work_cond;
    pthread_mutex_t* iteration_mutex;
    pthread_cond_t* iteration_cond;
    
    // Workload is shared by all threads
    struct workload* workload;
};

void do_work(int id, struct workload* w) {    
    uint8_t work_duration = 0;
    getrandom(&work_duration, sizeof(work_duration), 0);
    
    struct timespec duration = {
        .tv_sec = work_duration / 10,
        .tv_nsec = 0
    };
    
    nanosleep(&duration, NULL);
}

void* thread_work(void* p) {
    struct thread_spec* thspec = p;
    
    while(true) {
        int iteration = -1;
        
        //printf("iteration %i wait work %i...\n", thspec->iteration + 1, thspec->id);
        pthread_mutex_lock(thspec->iteration_mutex);
        while(thspec->iteration >= thspec->workload->iteration)
            pthread_cond_wait(thspec->iteration_cond, thspec->iteration_mutex);
        
        iteration = thspec->workload->iteration;
        pthread_mutex_unlock(thspec->iteration_mutex);
        
        printf("iteration %i start work %i\n", iteration, thspec->id);
        do_work(thspec->id, thspec->workload);
        printf("iteration %i end work %i\n", iteration, thspec->id);
        
        pthread_mutex_lock(&thspec->work_mutex);
        thspec->iteration = iteration;
        pthread_cond_broadcast(&thspec->work_cond);        
        pthread_mutex_unlock(&thspec->work_mutex);
    }
    
    return NULL;
}

int main() {
    struct workload w = {0};
    pthread_mutex_t iteration_mutex;
    pthread_cond_t iteration_cond;
    struct thread_spec specs[NUM_THREADS];
    
    printf("start\n");
    pthread_mutex_init(&iteration_mutex, NULL);
    pthread_cond_init(&iteration_cond, NULL);
    w.iteration = -1;

    for(int i = 0; i < NUM_THREADS; i++) {
        specs[i].id = i;
        specs[i].iteration = -1;
        specs[i].iteration_mutex = &iteration_mutex;
        specs[i].iteration_cond = &iteration_cond;
        specs[i].workload = &w;
        
        pthread_mutex_init(&specs[i].work_mutex, NULL);
        pthread_cond_init(&specs[i].work_cond, NULL);

        pthread_create(&specs[i].thread_id, NULL, thread_work, &specs[i]);
    }
    
    while(true) {
        pthread_mutex_lock(&iteration_mutex);
        w.iteration += 1;
        printf("start iteration %i\n", w.iteration);
        pthread_cond_broadcast(&iteration_cond);
        pthread_mutex_unlock(&iteration_mutex);

        // Wait work finish
        for(int i = 0; i < NUM_THREADS; i++) {
            pthread_mutex_lock(&specs[i].work_mutex);
            while(specs[i].iteration < w.iteration)
                pthread_cond_wait(&specs[i].work_cond, &specs[i].work_mutex);
            
            pthread_mutex_unlock(&specs[i].work_mutex);
        }
        printf("end iteration %i\n\n", w.iteration);
    }
} 
